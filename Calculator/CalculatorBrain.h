//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Daniel Dreier on 12/11/12.
//  Copyright (c) 2012 Daniel Dreier. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (double)performOperation:(NSString *)operation;

@end
