//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Daniel Dreier on 12/11/12.
//  Copyright (c) 2012 Daniel Dreier. All rights reserved.
//

#import "CalculatorBrain.h"
@interface CalculatorBrain();
@property (nonatomic, strong) NSMutableArray *opandStack;
@end

@implementation CalculatorBrain

@synthesize opandStack = _opandStack;

//getter with lazy instanciation
- (NSMutableArray *)opandStack
{
    if (_opandStack == nil ) _opandStack = [[NSMutableArray alloc] init];
    return _opandStack;
}

- (void)pushOperand:(double)operand;
{
    [self.opandStack addObject:[NSNumber numberWithDouble:operand]];
}

- (double)popOperand
{
    NSNumber *operandObject = [self.opandStack lastObject];
    if (operandObject) [self.opandStack removeLastObject];
    return [operandObject doubleValue];
}

- (double)performOperation:(NSString *)operation;
{
    double result = 0;

    // the problem with this approach is it only allows a stack that's two deep
    // to be processed - really need to loop through and process recursively
    if ([operation isEqualToString:@"+"]) {
        result = [self popOperand] + [self popOperand];
    } else if ([@"*" isEqualToString:operation]) {
        result = [self popOperand] * [self popOperand];
    } else if ([@"-" isEqualToString:operation]) {
        result = [self popOperand] - [self popOperand];
    } else if ([@"/" isEqualToString:operation]) {
        double divisor = [self popOperand];
        if (divisor) {
            result = [self popOperand] / divisor;
        } else {
            result = 0;
        }
    } else if ([@"π" isEqualToString:operation]) {
        result = 3.14159265359;
    } else if ([@"sin" isEqualToString:operation]) {
        result = sin([self popOperand]);
    } else if ([@"cos" isEqualToString:operation]) {
        result = cos([self popOperand]);
    } else if ([@"sqrt" isEqualToString:operation]) {
        result = sqrt([self popOperand]);
    } else if ([@"±" isEqualToString:operation]) {
        result = ([self popOperand]) * -1;
    } else if ([@"Clear" isEqualToString:operation]) {
        [self.opandStack removeAllObjects];
        result = 0;
    }
    
    [self pushOperand:result];
    return result;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"stack = %@", self.opandStack];
}

@end
