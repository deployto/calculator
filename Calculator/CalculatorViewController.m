//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Daniel Dreier on 12/11/12.
//  Copyright (c) 2012 Daniel Dreier. All rights reserved.
//

/*
Known issues:
 (none known)
 */

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic, strong) CalculatorBrain *brain;
@end

@implementation CalculatorViewController

@synthesize display = _display;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
@synthesize brain = _brain;
@synthesize history = _history;

- (CalculatorBrain *)brain
{
    if (!_brain) _brain = [[CalculatorBrain alloc] init];
    return _brain;
}


- (IBAction)digitPressed:(UIButton *)sender
{
    NSString *digit = sender.currentTitle;
    NSRange periodInTitle = [self.display.text rangeOfString:@"."];

    // Bail without doing anything if zero was pressed and one already exists
    if (([self.display.text isEqualToString:@"0"]) && ([digit isEqualToString:@"0"]))
    {
        return;
    }
    
    // Bail without doing anything if a period was pressed and one already exists
    if (([digit isEqualToString:@"."]) && (periodInTitle.location != NSNotFound))
    {
        return;
    }
    
    // Update display when user enters digits
    // Numbers replace the default 0, but decimals are appended to it
    if ((self.userIsInTheMiddleOfEnteringANumber) |= ([digit isEqualToString:@"."])) {
        self.display.text = [self.display.text stringByAppendingString:digit];
    } else {
        self.display.text = digit;
    }
    self.userIsInTheMiddleOfEnteringANumber = YES;
}

- (IBAction)enterPressed
// Sends values to the model's operand stack
{
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.history.text = [NSString stringWithFormat:@" %@ %@ ENT", self.history.text, self.display.text];
    self.userIsInTheMiddleOfEnteringANumber = NO;
}

- (IBAction)operationPressed:(UIButton *)sender
{
    if (self.userIsInTheMiddleOfEnteringANumber)
    {
        if ([sender.currentTitle isEqualToString:@"±"])
        {
        // +/- changes display text if entered with other numbers, and
        // as an operation if enter is pressed before it's called
            self.display.text = [NSString stringWithFormat:@"%g", (self.display.text.doubleValue * -1)];
            return;
        } else
        {
            // Triggered when user presses an operation key after entering numbers
            // but without hitting enter first -- presses enter for them prior to
            // performing operation
            [self enterPressed];
        }
    }
    
    // Request operation from brain model, then update display and history
    double result = [self.brain performOperation:sender.currentTitle];
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.history.text = [ NSString stringWithFormat:@"%@ %@ = %@", self.history.text, sender.currentTitle, resultString];
    self.display.text = resultString;

    // Make Clear button clear history
    if ([sender.currentTitle isEqualToString:@"Clear"])
    {
        self.history.text = @"";
        self.userIsInTheMiddleOfEnteringANumber = NO;
    }
}

- (IBAction)backPressed {
    // Removes the last character from the display string
    if (self.display.text.length)
    {
        self.display.text = [self.display.text substringToIndex:(self.display.text.length - 1)];
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
    // Zero should be displayed if the display string is reduced to zero length
    if (!self.display.text.length)
    {
        self.display.text = @"0";
        self.userIsInTheMiddleOfEnteringANumber = NO;
    }

}

@end
