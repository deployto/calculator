//
//  main.m
//  Calculator
//
//  Created by Daniel Dreier on 12/11/12.
//  Copyright (c) 2012 Daniel Dreier. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalculatorAppDelegate class]));
    }
}
