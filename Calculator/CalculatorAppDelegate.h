//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Daniel Dreier on 12/11/12.
//  Copyright (c) 2012 Daniel Dreier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
